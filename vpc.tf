resource "aws_vpc" "myvpc" {
  #cidr_block = "10.10.0.0/16"
  cidr_block = var.vpc_cidr
  #enable_dns_hostnames = "true"
  tags = {
    Name = var.vpc_name
  }
}

########Public-Subnets ##############

resource "aws_subnet" "mypublicsubnet" {
  vpc_id = aws_vpc.myvpc.id
  count  = length(data.aws_availability_zones.azs.names)
  #cidr_block              = "10.10.${count.index}.0/24"
  cidr_block              = "${var.cidr_block_public}.${count.index}.0/24"
  availability_zone       = data.aws_availability_zones.azs.names[count.index]
  map_public_ip_on_launch = "true"
  tags = {
    Name = "${var.vpc_name}-Public-Subnet-${1 + count.index}-${data.aws_availability_zones.azs.names[count.index]}"
  }
}

###########Private Subnet ######################
resource "aws_subnet" "myprivatecsubnet" {
  vpc_id     = aws_vpc.myvpc.id
  count      = length(data.aws_availability_zones.azs.names)
  cidr_block = "${var.cidr_block_public}.${10 + count.index}.0/24"
  #cidr_block = var.cidr_block_pvt
  availability_zone       = data.aws_availability_zones.azs.names[count.index]
  map_public_ip_on_launch = "true"
  tags = {
    Name = "${var.vpc_name}-Private-Subnet-${1 + count.index}-${data.aws_availability_zones.azs.names[count.index]}"
  }
}

### INTERNET

resource "aws_internet_gateway" "my-igw" {
  vpc_id = aws_vpc.myvpc.id
  tags = {
    Name = "${var.vpc_name}-IGW"
  }
}

##############RT
resource "aws_route_table" "mypublic" {
  vpc_id = aws_vpc.myvpc.id
  tags = {
    Name = "${var.vpc_name}-public-rt"
  }
}

resource "aws_route_table" "myprivate" {
  vpc_id = aws_vpc.myvpc.id
  tags = {
    Name = "${var.vpc_name}-private-rt"
  }
}


### CREATRE A ROUTE
resource "aws_route" "myroute" {
  route_table_id         = aws_route_table.mypublic.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.my-igw.id
}


### ASSOCIATE RT WITH SUBNET

resource "aws_route_table_association" "myrtassociation" {
  count          = length(aws_subnet.mypublicsubnet)
  subnet_id      = aws_subnet.mypublicsubnet[count.index].id
  route_table_id = aws_route_table.mypublic.id
}


resource "aws_route_table_association" "myprivatertassociation" {
  count          = length(aws_subnet.myprivatecsubnet)
  subnet_id      = aws_subnet.myprivatecsubnet[count.index].id
  route_table_id = aws_route_table.myprivate.id
}


############################## NAT G/W

resource "aws_nat_gateway" "mynatgw" {
  connectivity_type = "public"
  subnet_id         = aws_subnet.mypublicsubnet[0].id
  allocation_id     = aws_eip.my-eip.id
  tags = {
    Name = "${var.vpc_name}-my-nat-gw"
  }
}

##################
resource "aws_eip" "my-eip" {
  tags = {
    Name = "${var.vpc_name}-eip"
  }
}


### CREATRE A ROUTE FOR NAT
resource "aws_route" "mynatroute" {
  route_table_id         = aws_route_table.myprivate.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_nat_gateway.mynatgw.id
}

### CREATE SG

resource "aws_security_group" "mysg" {
  name   = "${var.vpc_name}-sg"
  vpc_id = aws_vpc.myvpc.id
  ingress {
    description = "allow port 22"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "allow port 80"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
    ingress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks =  ["0.0.0.0/0"]
  }
}
