data "aws_availability_zones" "azs" {
  state = "available"
}

# output "list_of_az" {
#   value = data.aws_availability_zones.azs[*].names
# }

data "aws_ami" "redhat-linux-9" {
  most_recent = true
  owners      = ["309956199498"]

  filter {
    name   = "name"
    values = ["RHEL-9.*.*_HVM-*-x86_64-49-Hourly2-GP3"]
  }
  filter {
    name   = "state"
    values = ["available"]
  }
  filter {
    name   = "root-device-type"
    values = ["ebs"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  filter {
    name   = "architecture"
    values = ["x86_64"]
  }
}
