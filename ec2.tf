resource "aws_instance" "myec2-ec2vm" {
  # count = length(aws_subnet.mypublicsubnet)
  # ami                    = "ami-0fe630eb857a6ec83"
  ami           = data.aws_ami.redhat-linux-9.id
  instance_type = "t2.medium"
  key_name      = "terraform"
  #subnet_id              = aws_subnet.mypublicsubnet[count.index].id
  subnet_id              = aws_subnet.mypublicsubnet[0].id
  vpc_security_group_ids = [aws_security_group.mysg.id]
  user_data              = file("apache-install.sh")
  tags = {
    # Name = "${var.vpc_name}-Public-${1 + count.index}"
    Name = "${var.vpc_name}-Public-Server"
  }
}
resource "aws_instance" "myec2-private" {
  # count = length(aws_subnet.mypublicsubnet)
  # ami                    = "ami-0fe630eb857a6ec83"
  ami           = data.aws_ami.redhat-linux-9.id
  instance_type = "t2.medium"
  key_name      = "terraform"
  #subnet_id              = aws_subnet.mypublicsubnet[count.index].id
  subnet_id              = aws_subnet.myprivatecsubnet[0].id
  vpc_security_group_ids = [aws_security_group.mysg.id]
  user_data              = file("apache-install.sh")
  tags = {
    # Name = "${var.vpc_name}-Public-${1 + count.index}"
    Name = "${var.vpc_name}-Private-Server"
  }
}